import csv
import os
import time

# Function to extract specific data from results.csv
def extract_data(file_path):
    data = []
    with open(file_path, newline='') as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            date_time = row[0].split()
            extracted_data = {
                'Date': date_time[0],
                'Time': date_time[1],
                'WPM': int(row[1]),
                'Accuracy': int(row[2]),
                'User': row[-1]
            }
            data.append(extracted_data)
    return data

def sort_and_write_data(data, filename):
    # Sort data by 'WPM', 'Accuracy', and 'User'
    sorted_data = sorted(data, key=lambda x: (-x['WPM'], -x['Accuracy'], x['User']))

    # Write sorted data to a new file
    with open(filename, 'w') as file:
        file.write("Date,Time,WPM,Accuracy,User\n")
        for item in sorted_data:
            file.write(f"{item['Date']},{item['Time']},{item['WPM']},{item['Accuracy']},{item['User']}\n")

def main():
    while True:
        # Directory where game.sh and folders are located
        base_dir = os.path.dirname(os.path.abspath(__file__))

        # Paths to normal and shuffled results.csv files
        normal_results_path = os.path.join(base_dir, 'normal', 'results.csv')
        shuffled_results_path = os.path.join(base_dir, 'shuffled', 'results.csv')

        # Extract data from normal and shuffled results.csv files
        normal_data = extract_data(normal_results_path)
        shuffled_data = extract_data(shuffled_results_path)

        # Sort and write extracted data to new files
        sort_and_write_data(normal_data, 'sorted_normal_data.csv')
        sort_and_write_data(shuffled_data, 'sorted_shuffled_data.csv')

        # Wait for 60 seconds before running the loop again
        time.sleep(60)

if __name__ == "__main__":
    main()

# Terminal based speed typing game

First do

```
chmod +x install.sh
```

Then start the script to install all in the right place.

## Start the Game
To start the game do
```
./game.sh
```

## Leaderboard
To have leaderboard you will have to run 
```
nohup python3 sort.py
```

```
nohup python3 web_leaderboard.py
```

## To kill web server
Do
killall -p python3
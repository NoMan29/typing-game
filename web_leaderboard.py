from flask import Flask, render_template
import csv

app = Flask(__name__)

def read_csv(filename):
    data = []
    with open(filename, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            data.append(row)
    return data

@app.route('/normal')
def normal_data():
    normal_data = read_csv('sorted_normal_data.csv')
    return render_template('data.html', title='Normal Data', data=normal_data)

@app.route('/shuffled')
def shuffled_data():
    shuffled_data = read_csv('sorted_shuffled_data.csv')
    return render_template('data.html', title='Shuffled Data', data=shuffled_data)

if __name__ == '__main__':
    app.run(debug=True)

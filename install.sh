#!/bin/bash

# Get Linux distribution name
get_linux_distro() {
    if [ -f /etc/os-release ]; then
        source /etc/os-release
        if [ -n "$ID" ]; then
            LINUX_DISTRO=$ID
        fi
    elif [ -f /etc/lsb-release ]; then
        source /etc/lsb-release
        if [ -n "$DISTRIB_ID" ]; then
            LINUX_DISTRO=$DISTRIB_ID
        fi
    fi
}

get_linux_distro

case "$LINUX_DISTRO" in
    "debian" | "ubuntu" | "pop" | "kali")
        echo "Detected Debian-based distribution."
        # Check if Python 3 is installed
        if ! command -v python3 &> /dev/null; then
            echo "Python 3 is not installed. Installing Python 3..."
            sudo apt update
            sudo apt install -y python3
            if [ $? -eq 0 ]; then
                echo "Python 3 installed successfully."
            else
                echo "Failed to install Python 3. Exiting..."
                exit 1
            fi
        fi

        # Check if pip is installed
        if ! command -v pip &> /dev/null; then
            echo "Pip is not installed. Installing Pip..."
            sudo apt install -y python3-pip
            if [ $? -eq 0 ]; then
                echo "Pip installed successfully."
            else
                echo "Failed to install Pip. Exiting..."
                exit 1
            fi
        fi

        # Check if typetest is already installed
        if ! python3 -c "import pkgutil; exit(not pkgutil.find_loader('typetest'))"; then
            echo "typetest module is not installed. Installing typetest..."
            pip install typetest
            if [ $? -eq 0 ]; then
                echo "typetest module installed successfully."
            else
                echo "Failed to install typetest module."
            fi
        else
            echo "typetest module is already installed."
        fi
        ;;
    "arch")
        echo "Detected Arch-based distribution."
        # Check if Python 3 is installed
        if ! command -v python3 &> /dev/null; then
            echo "Python 3 is not installed. Installing Python 3..."
            sudo pacman -Sy python
            if [ $? -eq 0 ]; then
                echo "Python 3 installed successfully."
            else
                echo "Failed to install Python 3. Exiting..."
                exit 1
            fi
        fi

        # Check if pip is installed
        if ! command -v pip &> /dev/null; then
            echo "Pip is not installed. Installing Pip..."
            sudo pacman -Sy python-pip
            if [ $? -eq 0 ]; then
                echo "Pip installed successfully."
            else
                echo "Failed to install Pip. Exiting..."
                exit 1
            fi
        fi

        # Check if typetest is already installed
        if ! python3 -c "import pkgutil; exit(not pkgutil.find_loader('typetest'))"; then
            echo "typetest module is not installed. Installing typetest..."
            pip install typetest
            if [ $? -eq 0 ]; then
                echo "typetest module installed successfully."
            else
                echo "Failed to install typetest module."
            fi
        else
            echo "typetest module is already installed."
        fi
        ;;
    "fedora")
        echo "Detected Fedora-based distribution."
        # Check if Python 3 is installed
        if ! command -v python3 &> /dev/null; then
            echo "Python 3 is not installed. Installing Python 3..."
            sudo dnf install -y python3
            if [ $? -eq 0 ]; then
                echo "Python 3 installed successfully."
            else
                echo "Failed to install Python 3. Exiting..."
                exit 1
            fi
        fi

        # Check if pip is installed
        if ! command -v pip &> /dev/null; then
            echo "Pip is not installed. Installing Pip..."
            sudo dnf install -y python3-pip
            if [ $? -eq 0 ]; then
                echo "Pip installed successfully."
            else
                echo "Failed to install Pip. Exiting..."
                exit 1
            fi
        fi

        # Check if typetest is already installed
        if ! python3 -c "import pkgutil; exit(not pkgutil.find_loader('typetest'))"; then
            echo "typetest module is not installed. Installing typetest..."
            pip install typetest
            if [ $? -eq 0 ]; then
                echo "typetest module installed successfully."
            else
                echo "Failed to install typetest module."
            fi
        else
            echo "typetest module is already installed."
        fi
        ;;
    *)
        echo "Unsupported or unknown distribution."
        exit 1
        ;;
esac

# Move typetest to the current directory
if [ -f "$HOME/.local/bin/typetest" ]; then
    echo "Moving typetest to the current directory..."
    mv "$HOME/.local/bin/typetest" ./
    if [ $? -eq 0 ]; then
        echo "typetest moved successfully."
    else
        echo "Failed to move typetest."
    fi
else
    echo "typetest not found in ~/.local/bin/ directory."
fi

chmod +x game.sh

echo "Done."
#!/bin/bash

while true; do
    # Clear the screen
    clear

    echo "Welcome to the Typing Competition!"
    echo "1. Speed Test"
    echo "2. Exit"

    read -p "Choose an option (1-2): " option

    case $option in
        1)
            while true; do
                # Ask user for a username
                read -p "Enter your username: " username

                # Check if the directory with the given username exists
                if [ -d "$username" ]; then
                    echo "Error: User '$username' already exists!"
                    read -n 1 -s -r -p "Press any key to continue..."
                else
                    echo "Welcome $username."
                    
                    while true; do
                        # Clear the screen
                        clear

                        echo "Speed Test:"
                        echo "1. Shuffled Words"
                        echo "2. Normal Mode"
                        echo "3. Back to Main Menu"

                        read -p "Choose an option (1-3): " speed_option

                        case $speed_option in
                            1)
                                while true; do
                                    # Clear the screen
                                    clear

                                    echo "Shuffled Words:"
                                    echo "1. About GNU Foundation (Shuffled)"
                                    echo "2. Arch (Shuffled)"
                                    echo "3. Gentoo (Shuffled)"
                                    echo "4. Back to Speed Test Menu"

                                    read -p "Choose an option (1-4): " shuffled_option

                                    case $shuffled_option in
                                        1)
                                            echo "Shuffled Words - About GNU Foundation selected"
                                            sleep 3
                                            cd shuffled
                                            ./typetest -d 30 -o "." -r 3 -i text/gnu --hash $username -s
                                            clear
                                            echo "Game finished, check score on the leaderboard"
                                            sleep 3
                                            rm -f char_speeds.csv mistyped_words.csv word_speeds.csv
                                            cd ..
                                            sleep 3
                                            ;;
                                        2)
                                            echo "Shuffled Words - Arch selected"
                                            sleep 3
                                            cd shuffled
                                            ./typetest -d 30 -o "." -r 3 -i text/arch --hash $username -s
                                            clear
                                            echo "Game finished, check score on the leaderboard"
                                            sleep 3
                                            rm -f char_speeds.csv mistyped_words.csv word_speeds.csv
                                            cd ..
                                            sleep 3
                                            ;;
                                        3)
                                            echo "Shuffled Words - Gentoo selected"
                                            sleep 3
                                            cd shuffled
                                            ./typetest -d 30 -o "." -r 3 -i text/gentoo --hash $username -s
                                            clear
                                            echo "Game finished, check score on the leaderboard"
                                            sleep 3
                                            rm -f char_speeds.csv mistyped_words.csv word_speeds.csv
                                            cd ..
                                            sleep 3
                                            ;;
                                        4)
                                            break
                                            ;;
                                        *)
                                            echo "Invalid option. Please choose a number between 1 and 4."
                                            read -n 1 -s -r -p "Press any key to continue..."
                                            ;;
                                    esac
                                done
                                ;;
                            2)
                                while true; do
                                    # Clear the screen
                                    clear

                                    echo "Normal Words:"
                                    echo "1. About GNU Foundation"
                                    echo "2. Arch"
                                    echo "3. Gentoo"
                                    echo "4. Back to Speed Test Menu"

                                    read -p "Choose an option (1-4): " normal_option

                                    case $normal_option in
                                        1)
                                            echo "About GNU Foundation selected"
                                            sleep 3
                                            cd normal
                                            ./typetest -d 30 -o "." -r 3 -i text/gnu --hash $username
                                            clear
                                            echo "Game finished, check score on the leaderboard"
                                            sleep 3
                                            rm -f char_speeds.csv mistyped_words.csv word_speeds.csv
                                            cd ..
                                            sleep 3

                                            ;;
                                        2)
                                            echo "Arch selected"
                                            sleep 3
                                            cd normal
                                            ./typetest -d 30 -o "." -r 3 -i text/arch --hash $username
                                            clear
                                            echo "Game finished, check score on the leaderboard"
                                            sleep 3
                                            rm -f char_speeds.csv mistyped_words.csv word_speeds.csv
                                            cd ..
                                            sleep 3
                                            ;;
                                        3)
                                            echo "Gentoo selected"
                                            sleep 3
                                            cd normal
                                            ./typetest -d 30 -o "." -r 3 -i text/gentoo --hash $username
                                            clear
                                            echo "Game finished, check score on the leaderboard"
                                            sleep 3
                                            rm -f char_speeds.csv mistyped_words.csv word_speeds.csv
                                            cd ..
                                            sleep 3
                                            ;;
                                        4)
                                            break
                                            ;;
                                        *)
                                            echo "Invalid option. Please choose a number between 1 and 4."
                                            read -n 1 -s -r -p "Press any key to continue..."
                                            ;;
                                    esac
                                done
                                ;;
                            3)
                                break
                                ;;
                            *)
                                echo "Invalid option. Please choose a number between 1 and 3."
                                read -n 1 -s -r -p "Press any key to continue..."
                                ;;
                        esac
                    done
                    break  # Exit the loop after successful execution
                fi
            done
            ;;
        2)
            # echo "Exiting..."
            # if [ -f "results.csv" ]; then
            #     rm "results.csv"
            # fi
            exit 0
            ;;
        *)
            echo "Invalid option. Please choose a number between 1 and 3."
            read -n 1 -s -r -p "Press any key to continue..."
            ;;
    esac
done